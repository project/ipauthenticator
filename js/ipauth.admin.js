
$(function(){

	$("#cancel").click(function() {
		history.back(1);
		//window.location = "/admin/settings/ipAuthenticator";
	});
	$("A.ip").click(function() {		
		$("#edit-ip1").val($("A.ip").html());	
	});

	/**
	 * This will perform an AJAX request to retreieve the users.
	 */

	//ipauth_filter = new ip_auth_filter();
	$('#user_filter').bind('keyup', function() {
		var value = $(this).val();

		// if there is no value to the input field, then get all ip authenticators
		if (!value.length)
			ip_auth_filter.get_all_users();

		// Filter authenticator list.
		if (value.length > 3)
			ip_auth_filter.get_authenticator_list(value);
	});
});


/**
 * This will create the filter plugin.
 */
(function() {
	var
		window = this,
		ip_auth_filter = window.ip_auth_filter = function( var_init ) {
			return new ip_auth_filter.fn.init( var_init );
		};

	ip_auth_filter.fn = ip_auth_filter.prototype = {
		init: function( var_init ) {
			return this;
		},

		get_all_users: function() {
			$.get('../../admin/user/ip_authenticator/data/get_auths/all', null, function(data, textStatus) {
				$('.form-table').html(data);
			});
		},

		get_authenticator_list: function( user_filter_text ) {
			//alert("get: "+user_filter_text);
			$.get('../../admin/user/ip_authenticator/data/get_auths/'+user_filter_text, null, function(data, textStatus) {
				$('.form-table').html(data);
			});
		}
	};
})();

ip_auth_filter.fn.init.prototype = ip_auth_filter.fn;
ip_auth_filter = ip_auth_filter();

